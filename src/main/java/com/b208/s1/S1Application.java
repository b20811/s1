package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Array;
import java.util.ArrayList;

@SpringBootApplication
@RestController
//It will tell springboot that this application will function as an endpoint handling web requests.
public class S1Application {
	//Annotation in Java Springboot marks classes for their intended functionalities
	//Springboot upon startup scans for classes and assigns behaviors based on their annotation.
	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);
	}

	//@GetMapping will map a route or an endpoint to acces or run our method.
	//When http://localhost:8080/hello is accessed from a client, we will be able to run the hi method
	@GetMapping("/hello")
	public String hello(){
		return "Hi from our Java Springboot App!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "John")String nameParameter){
		//Pass data through the url using our string query
		//http://localhost:8080/hi?name=Joe
		//We retrieve the value of the filed "name" from our URL
		//defaultValue is the fallback value when the query string or request param is empty
//		System.out.println(name);
		return "Hi! My name is " + nameParameter;
	}

	@GetMapping("/food")
	public String myFavoriteFood(@RequestParam(value = "food", defaultValue = "sashimi")String valueFood){
		return "Hi! My favorite food is " + valueFood;
	}
	//2 ways of passing data through the URL by using Java Springboot.
	//Query String using Request Params - Directly passing data into the url and getting the data
	//Path Variable is much more similar to ExpressJS's req.params

	@GetMapping("/greeting/{element}")
	public String greeting(@PathVariable("element") String nameParams){
		//@PathVariable() allows us to extract data directly from the url.
		return "Hello " + nameParams;
	}

	public ArrayList<String> enrollees = new ArrayList<>();

	public  String java (){
		return "Name: Java101, Schedule: MWF 8:00AM-11:00AM, Price: PHP 3000";
	}

	public String sql (){
		return "Name: SQL101, Schedule: TTH 8:00AM-11:00AM, Price: PHP 2000";
	}

	public String jsoop (){
		return "Name: JSOOP101, Schedule: Weekends 8:00AM-11:00AM, Price: PHP 2000";
	}


	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "name")String nameParams){
		enrollees.add(nameParams);
		return "Welcome " + nameParams;
	}

	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees(){
		return enrollees;
	}

	@GetMapping("/courses/{id}")
	public String getCourses(@PathVariable("id") String idParams){
		if(idParams.equalsIgnoreCase("java101")){
			return java();
		} else if(idParams.equalsIgnoreCase("sql101")){
			return sql();
		} else if(idParams.equalsIgnoreCase("jsoop101")){
			return jsoop();
		} else {
			return "Course cannot be found.";
		}
	}



}
